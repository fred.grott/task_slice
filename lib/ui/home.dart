import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_sidekick/flutter_sidekick.dart';

import 'package:task_slice/ui/new_task.dart';
import 'package:task_slice/models/task.dart';
import 'package:task_slice/dbhelper/manager.dart';
import 'package:task_slice/ui/timer.dart';

import 'package:highlighter_coachmark/highlighter_coachmark.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_slidable/flutter_slidable.dart';


class HomePage extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  HomePage({Key key, this.title}) : super(key: key);
  final String title;




  @override
  _HomePageState createState() => _HomePageState();


}



class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  Manager taskManager = Manager();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey _fabKey = const GlobalObjectKey("fab");
  final GlobalKey _itemKey = const GlobalObjectKey("item");

  SidekickController controller;


  @override
  void initState() {
    super.initState();
    taskManager.loadAllTasks();
    Timer(const Duration(seconds: 1), () => showCoachMarkFAB());
    controller = SidekickController(
      vsync: this,
      duration: const Duration(seconds: 1),
    );
  }

  @override
  void dispose() {
    // Don't forget to dispose the controller ;-).
    controller?.dispose();
    super.dispose();
  }


  void _showSnackBar(String msg) {
    final snackBar = SnackBar(content: Text(msg));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  // ignore: avoid_void_async
  void _startTimer(Task task) async {
    final Task result = await Navigator.push(
      context,
      platformPageRoute(
          builder: (context) => TimerPage(
            task: task,
          ), context: context),
    );
    if (result != null) {
      await Manager().updateTask(result);
      await taskManager.loadAllTasks();
      setState(() {
        _showSnackBar('Updated: ${result.title}');
      });
    }
  }

  // ignore: avoid_void_async
  void _addTask() async {
    final Task task = await Navigator.push(
      context,
      // I need to call this differently from this
      // platformPageRoute(builder: (context) => NewTaskPage(), context: context),
      // so need to change to this
      // Navigator.of(context).push(_createRoute());
      // define the fade in the _createRoute
      platformPageRoute(builder: (context) => NewTaskPage(), context: context),

    );

    if (task != null) {
      await Manager().addNewTask(task);
      await taskManager.loadAllTasks();
      setState(() {
        _showSnackBar('Added: ${task.title}');
      });
    }
  }

  // ignore: avoid_void_async
  void showCoachMarkFAB() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final fabCoach = prefs.getBool('fab') ?? false;
    if (fabCoach) {
      return;
    }

    final CoachMark coachMarkFAB = CoachMark();
    final RenderBox target = _fabKey.currentContext.findRenderObject() as RenderBox;

    Rect markRect = target.localToGlobal(Offset.zero) & target.size;
    markRect = Rect.fromCircle(
        center: markRect.center, radius: markRect.longestSide * 0.6);

    coachMarkFAB.show(
        targetContext: _fabKey.currentContext,
        markRect: markRect,
        children: [
          Center(
              child: PlatformText("Tap on button\nto add a tasks",
                  style: TextStyle(
                    fontSize: 24.0,
                    fontStyle: FontStyle.italic,
                    color: Colors.black,
                  )))
        ],
        duration: null,
        onClose: () {
          prefs.setBool('fab', true);
        });
  }

  // ignore: avoid_void_async
  void showCoachMarkItem() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final itemCoach = prefs.getBool('item') ?? false;
    if (_itemKey == null || _itemKey.currentContext == null || itemCoach) {
      return;
    }
    final CoachMark coachMarkFAB = CoachMark();

    final RenderBox target = _itemKey.currentContext.findRenderObject() as RenderBox;

    Rect markRect = target.localToGlobal(Offset.zero) & target.size;
    markRect = markRect.inflate(5.0);

    coachMarkFAB.show(
        targetContext: _itemKey.currentContext,
        markRect: markRect,
        markShape: BoxShape.rectangle,
        children: [
           Center(
              child: PlatformText("Select to start task\nSlide to delete or archive.",
                  style: TextStyle(
                    fontSize: 24.0,
                    fontStyle: FontStyle.italic,
                    color: Colors.black,
                  )))
        ],
        duration: null,
        onClose: () async {
          await prefs.setBool('item', true);
        });
  }

  @override
  Widget build(BuildContext context) {
    Timer(const Duration(seconds: 1), () => showCoachMarkItem());
    // Okay need to use PlatformScaffold with
    // FAB via MAterialScaffoldData and
    // different navbar icon for cupertino
    // eventually change this to platformbottomnavbar instead of
    // fab right now keep the plain scaffold
    // should use bottomnavbar for ios and android to go for
    // a consistent Ux look
    return Scaffold(
      key: _scaffoldKey,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.purpleAccent,
        key: _fabKey,
        onPressed: () {
          _addTask();
        },
        tooltip: 'Add new task',
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            // this is the below the appbar on home page and thus need white text for title
            SliverAppBar(
              expandedHeight: 80.0,
              floating: true,
              pinned: false,
              flexibleSpace: FlexibleSpaceBar(
                centerTitle: true,
                title: PlatformText(widget.title,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    )),
              ),
            ),
          ];
        },
        body: Container(
          child: StreamBuilder(
              stream: taskManager.tasksData.asStream(),
              // ignore: prefer_collection_literals
              initialData: List<Task>(),
              builder:
                  (BuildContext context, AsyncSnapshot<List<Task>> snapshot) {
                    final tasks = snapshot.data.reversed;

                    if (snapshot.connectionState != ConnectionState.done) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    }

                    if (tasks != null && tasks.isEmpty) {
                      return Center(
                        child: PlatformText(
                          'Cool! Nothing to do.',
                          style: const TextStyle(fontSize: 24),
                        ),
                      );
                    } else {
                      return ListView.builder(
                        padding: const EdgeInsets.only(top: 0),
                        itemCount: tasks.length,
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemBuilder: (BuildContext context, int index) {
                          final item = tasks.elementAt(index);
                          // need to change this to a sidekick
                          return Material(


                            key: index == 0 ? _itemKey : null,
                            // remove inkwell
                            child: InkWell(
                                child: TaskWidget(
                                  task: item,
                                  onRemoved: () async {
                                    await taskManager.loadAllTasks();
                                    setState(() {
                                      _showSnackBar('Removed: ${item.title}');
                                    });
                                  },
                                  onUpdated: () async {
                                    await taskManager.loadAllTasks();
                                    setState(() {
                                      _showSnackBar('Updated: ${item.title}');
                                    });
                                  },
                                  onTap: () {
                                    _startTimer(item);
                                  },
                                )),
                          );
                        },
                      );
                    }
                  })
      ),
    ));
  }
}


class TaskWidget extends StatefulWidget {
  final Task task;
  final VoidCallback onRemoved;
  final VoidCallback onUpdated;
  final VoidCallback onTap;

  // ignore: prefer_const_constructors_in_immutables
  TaskWidget({Key key, this.task, this.onRemoved, this.onUpdated, this.onTap})
      : super(key: key);

  @override
  _TaskWidgetState createState() => _TaskWidgetState();
}

class _TaskWidgetState extends State<TaskWidget> {
  @override
  Widget build(BuildContext context) {
    final task = widget.task;
    return Slidable(
      delegate: const SlidableDrawerDelegate(),
      actionExtentRatio: 0.25,
      actions: <Widget>[
        PlatformIconButton(
            icon: Icon(
              Icons.archive,
              size: 32.0,
              color: Colors.blue,
            ),
            onPressed: () async {
              final Task nTask = task..done = true;
              await Manager().updateTask(nTask);
              widget.onUpdated();
            }),
      ],
      secondaryActions: <Widget>[
        PlatformIconButton(
            icon: Icon(
              Icons.delete,
              size: 32.0,
              color: Colors.red,
            ),
            onPressed: () async {
              await Manager().removeTask(task);
              widget.onRemoved();
            }),
      ],
      child: Container(
        color: Colors.white,
        child: Material(
            child: Card(
                margin: const EdgeInsets.fromLTRB(4, 4, 4, 4),
                child: InkWell(
                    onTap: () {
                      widget.onTap();
                    },
                    child: Container(
                        height: 72.0,
                        margin: const EdgeInsets.fromLTRB(16, 4, 16, 4),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          // to leave room for the edge slide-able
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              task.done
                                  ? Icons.check_circle
                                  : Icons.check_circle_outline,
                              color: task.done ? Colors.green : Colors.red,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Hero(

                                      tag: 'text-${task.id}',
                                      child: PlatformText(
                                        task.title,
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      )),
                                  PlatformText(
                                    task.description,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                  PlatformText(
                                    "${task.pomCount} task_slice",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ))))),
      ),
    );
  }

}
