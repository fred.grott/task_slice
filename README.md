# task_slice

This is a flutter mobile application with a heavy dose of Material Design Motion UX 
demo-ing pomodoro task timing techniques used in task management.

# Videos

Some videos of the UX

## Youtube Videos

## Gifs


# Screenshot Collages

# UX Used
 
Apple Flat Design

Google Material Design

Google Material Design Motion UX

# OOP Used

Facory Dependency Injection

Widget Dependency Injection

Clean Arch

React

# License

BSD Clause 2 Copyright 2020 by fred Grott(Fredrick Allan Grott)

# Contact

See my gitlab user page site for contact info

[my gitliab user page site](https://fred.grott.gitlab.io)


